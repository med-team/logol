logol (1.7.9+dfsg-6) unstable; urgency=medium

  * d/tests: update tests to allow non-root testing and set DEBUG
    mode to ease failure analysis 
  * rebuild against new swi-prolog (Closes: #1026056)

 -- Olivier Sallou <osallou@debian.org>  Wed, 14 Dec 2022 08:24:54 +0000

logol (1.7.9+dfsg-5) unstable; urgency=medium

  * Team upload
  * Upload to unstable to accompany auto-biojava-live transition
    (Closes: #1009295)

 -- Pierre Gruet <pgt@debian.org>  Mon, 11 Apr 2022 16:38:53 +0200

logol (1.7.9+dfsg-4) experimental; urgency=medium

  * Team upload
  * Depending on biojava-live 1:1.9.5+dfsg, which introduced new jar names
  * Raising Standards version to 4.6.0:
    - Rules-Requires-Root: no

 -- Pierre Gruet <pgt@debian.org>  Sun, 10 Apr 2022 22:50:07 +0200

logol (1.7.9+dfsg-3) unstable; urgency=medium

  * d/control: add dep on swi-prolog ABI via  swi-prolog-abi-binary-68 

 -- Olivier Sallou <osallou@debian.org>  Fri, 04 Mar 2022 06:29:09 +0000

logol (1.7.9+dfsg-2) unstable; urgency=medium

  * Source only upload, rebuilding against swi-prolog/8.4.2+dfsg-2 after
    swi-prolog ABI break (Closes: #1006384).

 -- Olivier Sallou <osallou@debian.org>  Tue, 01 Mar 2022 08:54:23 +0000

logol (1.7.9+dfsg-1) unstable; urgency=medium

  [ Shruti Sridhar ]
  * Team Upload.
  * Add autopkgtests
  * Install README as docs

  [ Andreas Tille ]
  * Add hardening options
  * Use Files-Excluded to create clean tarball
  * Fix watch file
  * Watch file version 4
  * d/rules: Drop get-orig-source target
  * Skip blhc and reprotest in CI

 -- Shruti Sridhar <shruti.sridhar99@gmail.com>  Mon, 16 Aug 2021 16:00:11 +0530

logol (1.7.9-3) unstable; urgency=medium

  * Remove broken symlink (not needed) Closes: 988650 

 -- Olivier Sallou <osallou@debian.org>  Thu, 20 May 2021 06:10:11 +0000

logol (1.7.9-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Add salsa-ci file (routine-update)
  * Do not try to install Doc/logolParser.pdf since the according LaTeX
    file does not build any more
    Closes: #952294

 -- Andreas Tille <tille@debian.org>  Sun, 23 Feb 2020 17:58:32 +0100

logol (1.7.9-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  [ Olivier Sallou ]
  * Fix FTBS with swi-prolog 7.7.25 (Closes: #916953).

 -- Andreas Tille <tille@debian.org>  Thu, 20 Dec 2018 21:02:51 +0100

logol (1.7.8-3) unstable; urgency=medium

  * Team upload.
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Secure URI in copyright format
  * Remove trailing whitespace in debian/changelog
  * Remove trailing whitespace in debian/rules

 -- Andreas Tille <tille@debian.org>  Mon, 29 Oct 2018 09:05:37 +0100

logol (1.7.8-2) unstable; urgency=medium

  * Remove ncurses dependency (Closes: #804579)

 -- Olivier Sallou <osallou@debian.org>  Fri, 01 Jun 2018 09:23:38 +0000

logol (1.7.8-1) unstable; urgency=medium

  * New upstream release (bug fixes)

 -- Olivier Sallou <osallou@debian.org>  Wed, 04 Apr 2018 13:33:57 +0000

logol (1.7.6-1) unstable; urgency=medium

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Thu, 07 Dec 2017 16:51:33 +0000

logol (1.7.5-2) unstable; urgency=medium

  * Team upload.
  * Moved packaging from SVN to Git
  * cme fix dpkg-control
  * Standards-Version: 4.1.1
  * remove drmaa sym link as patches remove drmaa support
    while drmaa is back in Debian, new release is not compatible
    with current upstream code, Closes: #877461
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Sat, 07 Oct 2017 21:23:51 +0200

logol (1.7.5-1) unstable; urgency=medium

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Wed, 16 Nov 2016 13:24:20 +0100

logol (1.7.4-1) unstable; urgency=medium

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Tue, 12 Apr 2016 10:51:02 +0200

logol (1.7.2-3) unstable; urgency=medium

  * Team upload.
  * Depend on libmail-java instead of libgnumail-java
  [ Olivier Sallou ]
     - Fix antlr 3.5 parser generation issue (relates to #820286)
       Closes: #811185.

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 31 Jul 2015 17:17:23 +0200

logol (1.7.2-2) unstable; urgency=medium

  * d/patches/fix_swipl72: Fix swipl v7.x compatibility (Closes: #790136).

 -- Olivier Sallou <osallou@debian.org>  Mon, 29 Jun 2015 11:59:23 +0200

logol (1.7.2-1) unstable; urgency=medium

  * New upstream release
    d/control: use Standars 3.9.6

 -- Olivier Sallou <osallou@debian.org>  Thu, 05 Feb 2015 15:32:25 +0100

logol (1.7.0-2) unstable; urgency=medium

  * d/control,d/patches/remove_drmaa_support: Remove libdrmaa-java dependency
    due to gridengine removal from Jessie (#776131).
    d/series: remove drmaa patch
    d/README.Debian: explain non drmaa support of this release

 -- Olivier Sallou <osallou@debian.org>  Sun, 01 Feb 2015 08:56:52 +0100

logol (1.7.0-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * No source changes, rebuild against swi-prolog 6.6.5
  * Urgency high to not hold migration of swi-prolog for much longer

 -- Євгеній Мещеряков <eugen@debian.org>  Wed, 21 May 2014 21:26:00 +0200

logol (1.7.0-1) unstable; urgency=low

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Sat, 08 Feb 2014 13:25:38 +0100

logol (1.6.10-1) unstable; urgency=low

  * New upstream release, fix bug on Hamming error search

 -- Olivier Sallou <osallou@debian.org>  Sat, 08 Feb 2014 13:17:17 +0100

logol (1.6.9-3) unstable; urgency=low

  * d/control,d/patches: Remove rubygems dependency, thanks to
    Cédric Boutillier <boutil@debian.org> (Closes: #735579).
  * Upgrade to Standards 3.9.5.

 -- Olivier Sallou <osallou@debian.org>  Fri, 17 Jan 2014 09:07:38 +0100

logol (1.6.9-2) unstable; urgency=low

  * Fix prolog doc generation (Closes: #730916).

 -- Olivier Sallou <osallou@debian.org>  Sun, 01 Dec 2013 11:55:42 +0100

logol (1.6.9-1) unstable; urgency=low

  * New upstream release (bug fixes, add Windows support
    for Sicstus 4.2.x and Swi-prolog).

 -- Olivier Sallou <osallou@debian.org>  Mon, 11 Nov 2013 10:11:17 +0100

logol (1.6.6-1) unstable; urgency=low

  * New upstream releas (bug fixes on morphisms and doc).

 -- Olivier Sallou <osallou@debian.org>  Wed, 30 Oct 2013 16:21:09 +0100

logol (1.6.5-1) unstable; urgency=low

  * New upstream release (memory footprint optimisation in some cases).

 -- Olivier Sallou <osallou@debian.org>  Fri, 25 Oct 2013 13:48:22 +0200

logol (1.6.4-1) unstable; urgency=low

  * New upstream release fixing bugs

 -- Olivier Sallou <osallou@debian.org>  Tue, 22 Oct 2013 08:35:45 +0200

logol (1.6.3-1) unstable; urgency=low

  * New upstream release fixing important bugs on distance search
  * Remove patch for doc generation after swi-prolog fix

 -- Olivier Sallou <osallou@debian.org>  Mon, 02 Sep 2013 08:54:46 +0200

logol (1.6.2-3) unstable; urgency=low

  * Fix swi-prolog compilation (Closes: #719030).

 -- Olivier Sallou <osallou@debian.org>  Thu, 08 Aug 2013 10:30:12 +0200

logol (1.6.2-2) unstable; urgency=low

  [ Olivier Sallou ]
  * Remove deprecated DM-Upload field.

  [ Dmitrijs Ledkovs ]
  * Bump standards version to 3.9.4.
  * Remove empty postinst script.
  * Add dependancy on texlive-latex-extra, needed to fix documentation
    FTBFS with new TeX Live.

 -- Dmitrijs Ledkovs <dmitrij.ledkov@ubuntu.com>  Mon, 03 Jun 2013 23:36:49 +0100

logol (1.6.2-1) unstable; urgency=low

  * New upstream release (bug fixes)

 -- Olivier Sallou <osallou@debian.org>  Fri, 24 May 2013 14:37:24 +0200

logol (1.6.1-1) unstable; urgency=low

  * New upstream release (bug fixes)

 -- Olivier Sallou <osallou@debian.org>  Tue, 09 Apr 2013 16:57:33 +0200

logol (1.6.0-1) unstable; urgency=low

  * New upstream release with bug fixes

 -- Olivier Sallou <osallou@debian.org>  Fri, 15 Feb 2013 10:05:50 +0100

logol (1.5.0-6) unstable; urgency=low

  [ Olivier Sallou ]
  * debian/control: Fix typo in description (Closes: #685079).
    Thanks to Erik Esterer <erik.esterer@gmail.com>

 -- Olivier Sallou <osallou@debian.org>  Fri, 17 Aug 2012 08:00:58 +0200

logol (1.5.0-5) unstable; urgency=low

  [ Olivier Sallou]
  * Transition package to use default java implementation:
    From james.page@ubuntu.com (Closes: #684158)
    - d/control: BD on default-jdk (>= 1:1.6), switch runtime dependency
      to default-jre | java6-runtime.
    - d/rules: Specify source/target = 1.5 to ensure backwards compatible
      bytecode is built.

 -- Olivier Sallou <osallou@debian.org>  Tue, 07 Aug 2012 15:18:43 +0200

logol (1.5.0-4) unstable; urgency=low

  * debian/postinst: remove directory permissions (Closes: #683647).

 -- Olivier Sallou <osallou@debian.org>  Thu, 02 Aug 2012 17:09:31 +0200

logol (1.5.0-2) unstable; urgency=low

  * Support Arch dependent and arch independent separate builds.

 -- Olivier Sallou <osallou@debian.org>  Sun, 01 Jul 2012 23:49:16 +0200

logol (1.5.0-1) unstable; urgency=low

  * Initial release (Closes: #678373).

 -- Olivier Sallou <osallou@debian.org>  Thu, 21 Jun 2012 12:50:22 +0200
